@extends('master')
@section('pertanyaan-tittle')
<h1>Pertanyaan</h1>
@endsection
@section('pertanyaan')
<button class="btn btn-success" style="margin-bottom:20px" data-toggle="modal" data-target="#pertanyaan"><b>+ Buat Pertanyaan</b></button>

<div class="modal fade" id="pertanyaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/pertanyaan/post" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Buat Pertanyaan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Judul</label>
                        <input type="text" class="form-control" name="judul" id="exampleFormControlInput1" placeholder="Laravel">
                        @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Isi pertanyaan</label>
                        <textarea class="form-control" name="isi" id="exampleFormControlTextarea1" rows="10"></textarea>
                        @error('isi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>

<table>
    @forelse ($questions as $key=>$value)
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{$value->judul}}</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                <a href="/pertanyaan/delete/{{$value->id}}">
                    <button type="button" class="btn btn-tool">
                        <i class="fas fa-times"></i></button>
                </a>
            </div>
        </div>
        <div class="card-body">
            {{$value->isi}}
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button class="btn btn-warning" data-toggle="modal" data-target="#ubah-pertanyaan" data-id="{{$value->id}}" data-judul="{{$value->judul}}" data-isi="{{$value->isi}}" id="edit-pertanyaan"><b>Ubah Pertanyaan</b></button>
        </div>
        <!-- /.card-footer-->
    </div>
    <!-- /.card -->
    @empty
    <p>No Data</p>
    @endforelse
</table>

<div class="modal fade" id="ubah-pertanyaan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/pertanyaan/update" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Pertanyaan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="id" id="id" style="visibility:hidden;position:absolute">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Judul</label>
                        <input type="text" class="form-control" name="judul" id="judul" placeholder="Laravel">
                        @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Isi pertanyaan</label>
                        <textarea class="form-control" name="isi" id="isi" rows="10"></textarea>
                        @error('isi')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@push('data-edit')
<script>
    $(document).on("click", "#edit-pertanyaan", function() {
        var id = $(this).data('id');
        var judul = $(this).data('judul');
        var isi = $(this).data('isi');
        $("#ubah-pertanyaan #id").val(id);
        $("#ubah-pertanyaan #judul").val(judul);
        $("#ubah-pertanyaan #isi").val(isi);
    })
</script>
@endpush