<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PertanyaanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/tabletask', [HomeController::class, 'tabletask']);
Route::get('/datatable', [HomeController::class, 'datatable']);
Route::get('/pertanyaan', [PertanyaanController::class, 'index']);
Route::post('/pertanyaan/post', [PertanyaanController::class, 'post']);
Route::post('/pertanyaan/update', [PertanyaanController::class, 'update']);
Route::get('/pertanyaan/delete/{id}', [PertanyaanController::class, 'delete']);
