<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('master');
    }

    public function datatable()
    {
        return view('v_datatable');
    }

    public function tabletask()
    {
        return view('v_tabletask');
    }
}
