<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    public function index()
    {
        $questions = DB::table('questions')->get();
        return view('v_pertanyaan', compact('questions'));
    }

    public function post(Request $pertanyaan)
    {
        $pertanyaan->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        $query = DB::table('questions')->insert([
            "judul" => $pertanyaan["judul"],
            "isi" => $pertanyaan["isi"],
            "profil_id" => 1
        ]);
        return redirect('/pertanyaan');
    }

    public function update(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        $query = DB::table('questions')
            ->where('id', $request["id"])
            ->update([
                'judul' => $request["judul"],
                'isi' => $request["isi"]
            ]);
        return redirect('/pertanyaan');
    }

    public function delete($id)
    {
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
